moveUnit(u : Unit , dest : Tile){
	moveType = getMoveType(u,t);
	if( moveType == invalidMove){
		return;
	}else if( moveType == CombineUnit ){
		combineUnits(dest.getUnit(), u); // combineUnits(u1,u2) replaces u1 with combo of u1,u2
	}else{
		if( (dest.getControllingPlayer != NULL) && (dest.getControllingPlayer() != u.myVillage.controlledBy()) ){
			if(dest.getUnit != NULL){
				dest.getUnit.kill();
			}
		}
		// ACTUALLY MOVE THE UNIT
		oldTile = u.getTile();
		oldTile.setUnit(NULL);
		dest.setUnit(u);
		u.setTile(dest);
		if(moveType == trampleMeadow){
			trampleMeadow(u);
		}else if(moveType == clearTomb){
			clearTomb(u);
		}else if(moveType == gatherWood){
			gatherWood(u);
		}
		if(dest.getControllingPlayer() == NULL){
			combineVillages();
			int currentTurn = this.gameState.getRoundCount();
			u.setImmobileUnitRound(currentTurn + 1)
			u.setCurrentAction(MOVED);
		}else if(dest.getControllingPlayer() != u.myVillage.controlledBy ){
			takeOverTile(dest);
			int currentTurn = this.gameState.getRoundCount();
			u.setImmobileUnitRound(currentTurn + 1)
			u.setCurrentAction(MOVED);
		}
	}
}
getMoveType(u : unit , dest: Tile){
	Tile t = u.getTile();
	Sequence<Tile> adjacent = t.getNeigbors();
	if( dest in adjacent ){
		TerrainType land = dest.getTerrainType();
		Structure struct = dest.getStructure();
		Unit onDest = dest.getUnit();
		Village village = dest.getVillage();
		if ( onDest != NULL ){
			//Combine units
			if( onDest.getControllingPlayer() == u.myVillage.controlledBy()){
				return CombineUnit;
			}else{
				//Enemy Stronger
				if( Unit.unitLevel(onDest.getUnitType()) > Unit.unitLevel(u.getUnitType())){
					return invalidMove;
				}
			}
		}
		if (village != NULL){
			//Unit invades enemy village
			if( (u.unitType == knight or u.unitType == soldier) and village.controlledBy != u.myVillage.controlledBy){
				return regularMove;
			}else{
				return invalidMove;
			}
		}
		if( land == "Tree"){
			if( u.unitType == knight ){
				return invalidMove;
			}else{
				return gatherWood;
			}
		}
		if ( struct == "Tomb"){
			if( u.unitType == knight ){
				return invalidMove;
			}else{
				return clearTomb;
			}
		}
		if( land == "Meadow"){
			if( u.unitType == knight OR u.unitType == soldier){
				return trampleMeadow;
			}else{
				return regularMove;
			}
		}
		if (land == "Sea"){
			return invalidMove;
		}
	}else{
		return invalidMove
	}
}
combineUnits(u1 : Unit, u2 : Unit){
	UnitType ret;
	if(u1.getUnitType() == peasant && u2.getUnitType() == peasant ){
		ret = infantry;
	}else if(u1.getUnitType() == infantry && u2.getUnitType() == infantry ){
		ret = knight;
	}else if( (u1.getUnitType() == peasant && u2.getUnitType() == infantry) || (u1.getUnitType() == infantry && u2.getUnitType() == peasant)){
		ret = soldier;
	}else if( (u1.getUnitType() == peasant && u2.getUnitType() == soldier) || (u1.getUnitType() == soldier && u2.getUnitType() == peasant)){
		ret = knight;
	}else{
		return;
	}
	u1.setUnitType(ret);
	u2.kill();
	return;
}
combineVillages(u : unit){
	Sequence<Tile> adj = u.getTile().getNeigbors();
	for( Tile t in adj){
		for( Tile t' in adj){
			if( t != t'){
				if(t.getControllingPlayer() == t'.getControllingPlayer(){
					if(villageLevel( t.getRegion().getVillage()) > villageLevel(t'.getRegion().getVillage())){
						Village oldV = t'.getVillage();
						Region oldR = t'.getRegion();
						for( Tile aTile in t'.getRegion.getTiles()){
							aTile.setRegion(t.getRegion());
						}
						t.transactWood(oldV.getWood());
						t.transactGold(oldV.getGold());
						oldV.destroy();
						oldR.destroy();
					}else{
						Village oldV = t.getVillage();
						Region oldR = t.getRegion();
						for( Tile aTile in t.getRegion.getTiles()){
							aTile.setRegion(t'.getRegion());
						}
						t'.transactWood(oldV.getWood());
						t'.transactGold(oldV.getGold());
						oldV.destroy();
						oldR.destroy();
					}
				}
			}
		}
	}
}